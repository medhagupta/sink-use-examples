import QtQuick 2.2
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

Kirigami.ApplicationWindow {
    id: root

    title: "Sink Calendars"

    width: Kirigami.Units.gridUnit * 25
    height: Kirigami.Units.gridUnit * 15

    pageStack.initialPage: mainPageComponent

    Component {
        id: mainPageComponent

        Kirigami.Page {
            title: i18n("Sink Calendars")

            ColumnLayout {
                spacing: 10
                anchors.fill: parent
                
                Button {
                    Layout.fillWidth: true
                    icon.name: "go-next"
                    text: i18n("create caldav resource")
                    onClicked: {
                        hello.createResource();
                    }
                }

                Button {
                    Layout.fillWidth: true
                    icon.name: "go-next"
                    text: i18n("sync events")
                    onClicked: {
                        hello.synchCalendar();
                    }
                }

                Button {
                    Layout.fillWidth: true
                    icon.name: "go-next"
                    text: i18n("count events")
                    onClicked: {
                        hello.countEvents();
                    }
                }
            }
        }
    }
}
