#include <sink/store.h>
#include <QObject>

using namespace Sink;
using namespace Sink::ApplicationDomain;
using Sink::ApplicationDomain::SinkResource;

class SyncCalendar : public QObject
{
    Q_OBJECT
    QByteArray mResourceInstanceIdentifier;

public:
    SyncCalendar();
    ~SyncCalendar();

public Q_SLOTS:
    void createResource();
    void synchCalendar();
    void countEvents();
};