#include <sink/store.h>
#include <sink/secretstore.h>
#include <sink/resourcecontrol.h>
#include "synccontacts.h"
#include <QDebug>

#include <QUuid>
#include <KCalCore/Event>
#include <KCalCore/ICalFormat>

SyncContacts::SyncContacts()
{
}

SyncContacts::~SyncContacts()
{
}

void SyncContacts::createContact()
{   
    auto resource = Sink::ApplicationDomain::CardDavResource::create("account1");

    resource.setProperty("server","https://server-address");
    resource.setProperty("username", "username");
    Sink::SecretStore::instance().insert(resource.identifier(), "*****");
    Store::create(resource).exec().waitForFinished();
    qDebug()<<"***CONTACT CREATED***";
    
    mResourceInstanceIdentifier = resource.identifier();
    return;
}

void SyncContacts::synchContact()
{
    //start resourcecontrol
    Sink::ResourceControl::start(mResourceInstanceIdentifier);

    //Sync Addressbooks
    Sink::SyncScope scope1;
    scope1.setType<Addressbook>();
    scope1.resourceFilter(mResourceInstanceIdentifier);
    Store::synchronize(scope1).exec().waitForFinished();

    //flush
    Sink::ResourceControl::flushMessageQueue(mResourceInstanceIdentifier).exec().waitForFinished();
    qDebug()<<"***FLUSHED***";

    //Sync Contacts
    Sink::SyncScope scope2;
    scope2.setType<Sink::ApplicationDomain::Contact>();
    scope2.resourceFilter(mResourceInstanceIdentifier);
    Store::synchronize(scope2).exec().waitForFinished();
    qDebug()<<"***CONTACT SYNCED***";

    //flush
    Sink::ResourceControl::flushMessageQueue(mResourceInstanceIdentifier).exec().waitForFinished();
    qDebug()<<"***FLUSHED***";

    return;
}

void SyncContacts::countContacts()
{
    const auto contacts = Sink::Store::read<Sink::ApplicationDomain::Contact>(Sink::Query().resourceFilter(mResourceInstanceIdentifier));
    // QCOMPARE(contacts.size(), 2);
    qDebug()<<" C  O  N  T  A  C  T  S  :  "<<contacts.size();
    return;
}
