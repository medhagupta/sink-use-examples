#include <sink/store.h>
#include <QObject>

using namespace Sink;
using namespace Sink::ApplicationDomain;
using Sink::ApplicationDomain::SinkResource;

class SyncContacts : public QObject
{
    Q_OBJECT
    QByteArray mResourceInstanceIdentifier;

public:
    SyncContacts();
    ~SyncContacts();

public Q_SLOTS:
    void createContact();
    void synchContact();
    void countContacts();
};